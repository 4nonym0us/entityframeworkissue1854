using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFrameworkIssue1854.Models
{
    public class Mechanic2Fix
    {
        public int Id { get; set; }

        [Key]
        public int MechanicId { get; set; }

        [ForeignKey("MechanicId")]
        public Mechanic Mechanic { get; set; }

        [Key]
        public int FixId { get; set; }

        [ForeignKey("FixId")]
        public Fix Fix { get; set; }
    }
}