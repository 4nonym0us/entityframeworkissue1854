﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkIssue1854.Models
{
    public class Mechanic
    {
        public Mechanic()
        {
            FixesLink = new List<Mechanic2Fix>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        public string Name { get; set; }

        public string Phone { get; set; }
        public string Address { get; set; }

        [DataType(DataType.Currency)]
        [Range(0.0, double.MaxValue, ErrorMessage = "{0} must be bigger then 0.")]
        public double HourRate { get; set; }

        public virtual ICollection<Mechanic2Fix> FixesLink { get; set; }
    }


    public class MechanicDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}