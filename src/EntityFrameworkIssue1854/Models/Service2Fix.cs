using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkIssue1854.Models
{
    public class Service2Fix
    {
        public int Id { get; set; }

        [Key]
        public int RepairServiceId { get; set; }

        public virtual RepairService RepairService { get; set; }

        [Key]
        public int FixId { get; set; }

        public virtual Fix Fix { get; set; }
    }
}