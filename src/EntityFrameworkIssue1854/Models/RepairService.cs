﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkIssue1854.Models
{
    public class RepairService
    {
        public RepairService()
        {
            FixesLink = new List<Service2Fix>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        public string Name { get; set; }

        [DataType(DataType.Currency)]
        [Required(ErrorMessage = "{0} is required")]
        [Range(0.0, double.MaxValue, ErrorMessage = "{0} must be bigger then 0.")]
        public double Price { get; set; }

        public virtual ICollection<Service2Fix> FixesLink { get; set; }
    }

    public class RepairServiceDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string[] FixesLink { get; set; }
    }
}