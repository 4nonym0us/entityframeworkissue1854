﻿using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkIssue1854.Models
{
    public class Part2Fix
    {
        public int Id { get; set; }

        [Required]
        public int UsedPartsCount { get; set; }

        [Key]
        public int PartId { get; set; }

        public virtual Part Part { get; set; }

        [Key]
        public int FixId { get; set; }

        public virtual Fix Fix { get; set; }
    }

    public class Part2FixJoinFixDto
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public virtual PartDto Part { get; set; }
    }
}