﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace EntityFrameworkIssue1854.Models
{
    public class Fix
    {
        public Fix()
        {
            MechanicsLink = new List<Mechanic2Fix>();
            PartsLink = new List<Part2Fix>();
            ServicesLink = new List<Service2Fix>();
        }

        [ScaffoldColumn(false)]
        public int Id { get; set; }

        public bool IsFlatRate { get; set; }
        public bool IsTruck { get; set; }
        public bool IsPaid { get; set; }

        [DataType(DataType.Currency)]
        public double FlatRate { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        public double Labour { get; set; }

        public double Milage { get; set; }
        public string Note { get; set; }

        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required")]
        public DateTime Date { get; set; }

        public int? TruckId { get; set; }
        public virtual Truck Truck { get; set; }
        public int? TrailerId { get; set; }
        public virtual Trailer Trailer { get; set; }
        public virtual ICollection<Mechanic2Fix> MechanicsLink { get; set; }
        public virtual ICollection<Part2Fix> PartsLink { get; set; }
        public virtual ICollection<Service2Fix> ServicesLink { get; set; }

        public double CalculatePrice()
        {
            var service = ServicesLink.Select(s => s.RepairService);
            var partsPrice = PartsLink.Sum(link => link.UsedPartsCount*link.Part.SellPrice);
            return IsFlatRate ? FlatRate : partsPrice + Labour * service.Sum(p => p.Price);
        }
    }

    public class FixDto
    {
        public int Id { get; set; }
        public bool IsFlatRate { get; set; }
        public bool IsTruck { get; set; }
        public bool IsPaid { get; set; }
        public double FlatRate { get; set; }
        public double Labour { get; set; }
        public double Milage { get; set; }
        public string Note { get; set; }
        public DateTime Date { get; set; }
        public double Price { get; set; }

        public virtual Truck Truck { get; set; }
        public virtual Trailer Trailer { get; set; }
        public virtual IEnumerable<MechanicDto> Mechanics { get; set; }
        public virtual IEnumerable<RepairServiceDto> Services { get; set; }
        public virtual ICollection<Part2FixJoinFixDto> Parts { get; set; }
    }
}