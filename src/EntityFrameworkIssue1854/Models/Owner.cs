﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EntityFrameworkIssue1854.Models;

namespace FreightBusinessSuite.Models
{
    public class Owner
    {
        public Owner()
        {
            Trucks = new List<Truck>();
            Trailers = new List<Trailer>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        public string Name { get; set; }

        public string Address { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        public virtual ICollection<Truck> Trucks { get; set; }
        public virtual ICollection<Trailer> Trailers { get; set; }
    }
}