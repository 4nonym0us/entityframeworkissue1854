﻿using FreightBusinessSuite.Models;
using Microsoft.Data.Entity;
using Microsoft.Framework.ConfigurationModel;

namespace EntityFrameworkIssue1854.Models
{
    public class FbsDbContext : DbContext
    {
        public DbSet<Fix> Fixes { get; set; }
        public DbSet<Make> Makers { get; set; }
        public DbSet<Mechanic> Mechanics { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Part> Parts { get; set; }
        public DbSet<RepairService> RepairServices { get; set; }
        public DbSet<Trailer> Trailers { get; set; }
        public DbSet<Truck> Trucks { get; set; }
        public DbSet<TruckType> TruckTypes { get; set; }

        public DbSet<Service2Fix> Service2Fixes { get; set; }
        public DbSet<Part2Fix> Part2Fixes { get; set; }
        public DbSet<Mechanic2Fix> Mechanic2Fixes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = new Configuration().AddJsonFile("config.json")
                .Get("Data:DefaultConnection:ConnectionString");
            optionsBuilder.UseSqlServer(connectionString);

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Fix>()
                .Collection(p => p.MechanicsLink).InverseReference(c => c.Fix)
                .ForeignKey(c => c.FixId);
            builder.Entity<Fix>()
                .Collection(p => p.PartsLink).InverseReference(c => c.Fix)
                .ForeignKey(c => c.FixId);
            builder.Entity<Fix>()
                .Collection(p => p.ServicesLink).InverseReference(c => c.Fix)
                .ForeignKey(c => c.FixId);

            builder.Entity<Part>()
                .Collection(p => p.FixesLink).InverseReference(c => c.Part)
                .ForeignKey(c => c.PartId);

            builder.Entity<Mechanic>()
                .Collection(p => p.FixesLink).InverseReference(c => c.Mechanic)
                .ForeignKey(c => c.MechanicId);

            builder.Entity<RepairService>()
                .Collection(p => p.FixesLink).InverseReference(c => c.RepairService)
                .ForeignKey(c => c.RepairServiceId);

            base.OnModelCreating(builder);
        }
    }
}