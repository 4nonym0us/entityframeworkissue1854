﻿using System.ComponentModel.DataAnnotations;

namespace EntityFrameworkIssue1854.Models
{
    public class Make
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        public string Name { get; set; }
    }
}