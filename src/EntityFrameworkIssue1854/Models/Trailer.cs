﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FreightBusinessSuite.Models;

namespace EntityFrameworkIssue1854.Models
{
    public class Trailer
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Range(0.0, int.MaxValue, ErrorMessage = "{0} must be bigger then 0.")]
        public int Number { get; set; }

        public int? OwnerId { get; set; }
        public virtual Owner Owner { get; set; }
        public int? TruckId { get; set; }
        public virtual Truck Truck { get; set; }
        public virtual ICollection<Fix> Fixes { get; set; }
    }
}