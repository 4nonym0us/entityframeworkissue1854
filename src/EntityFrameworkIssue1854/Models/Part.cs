﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace EntityFrameworkIssue1854.Models
{
    public class Part
    {
        public Part()
        {
            FixesLink = new List<Part2Fix>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Range(0.0, int.MaxValue, ErrorMessage = "{0} must be bigger then 0.")]
        public int Number { get; set; }

        public string Description { get; set; }

        [DataType(DataType.Currency)]
        [Required(ErrorMessage = "{0} is required")]
        [Range(0.0, double.MaxValue, ErrorMessage = "{0} must be bigger then 0.")]
        public double BuyPrice { get; set; }

        [DataType(DataType.Currency)]
        [Required(ErrorMessage = "{0} is required")]
        [Range(0.0, double.MaxValue, ErrorMessage = "{0} must be bigger then 0.")]
        public double SellPrice { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Range(1.0, int.MaxValue, ErrorMessage = "{0} must be bigger not less then 1.")]
        public int Quantity { get; set; }

        public DateTime Date { get; set; }

        public virtual ICollection<Part2Fix> FixesLink { get; set; }

        public int GetAmountLeft()
        {
            return FixesLink.Sum(x => x.UsedPartsCount);
        }
    }



    public class PartDto
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public double SellPrice { get; set; }
    }
}