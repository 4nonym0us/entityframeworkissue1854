﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using FreightBusinessSuite.Models;

namespace EntityFrameworkIssue1854.Models
{
    public class Truck
    {
        public Truck()
        {
            Trailers = new List<Trailer>();
            Fixes = new List<Fix>();
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Range(0.0, int.MaxValue, ErrorMessage = "{0} must be bigger then 0.")]
        public int Number { get; set; }

        [Range(0.0, double.MaxValue, ErrorMessage = "{0} must be bigger then 0.")]
        public double Milage { get; set; }

        [MinLength(17, ErrorMessage = "VIN number has 17-digit length"),
         MaxLength(17, ErrorMessage = "VIN number has 17-digit length")]
        public string VinNumber { get; set; }

        [Range(0, double.MaxValue, ErrorMessage = "{0} must be bigger then 0.")]
        public double TireSize { get; set; }

        public int? TruckTypeId { get; set; }
        public virtual TruckType TruckType { get; set; }
        public int? OwnerId { get; set; }
        public virtual Owner Owner { get; set; }
        public virtual ICollection<Fix> Fixes { get; set; }
        public virtual ICollection<Trailer> Trailers { get; set; }
    }
}