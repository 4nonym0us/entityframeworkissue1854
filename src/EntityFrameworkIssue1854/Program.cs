﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityFrameworkIssue1854.Models;
using FreightBusinessSuite.Models;

namespace EntityFrameworkIssue1854
{
    public class Program
    {
        public void Main(string[] args)
        {
            Console.WriteLine("creating sample data...");
            CreateSampleData();
            Console.WriteLine("data was created");

            var fixes = GetData();
            Console.WriteLine($"received {fixes.Count()} items:");

            foreach (var fix in fixes)
            {
                Console.WriteLine($"fixId: {fix.Id}");
            }

            Console.WriteLine("exiting");
            Console.ReadLine();
        }

        public List<Fix> GetData()
        {
            try
            {
                using (var context = new FbsDbContext())
                {
                    var query = context.Set<Fix>()
                        .Include(x => x.Truck)
                        .Include(x => x.Trailer)
                        .Include(x => x.PartsLink)
                        .Include(x => x.ServicesLink).ThenInclude(s => s.RepairService) // this ThenInclude() invocation triggers an exception
                        .Include(x => x.MechanicsLink).ThenInclude(m => m.Mechanic);
                    return query.ToList();
                }
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.StackTrace);
                return new List<Fix>();
            }
        }

        private static void CreateSampleData()
        {
            using (var dbContext = new FbsDbContext())
            {
                dbContext.Database.EnsureDeleted();
                dbContext.Database.EnsureCreated();

                // add some data
                var mechanics = new List<Mechanic>
                        {
                            new Mechanic {Name =  "Sharon Castillo", Address =  "Ap #848-4672 Imperdiet Ave", Phone =  "(562) 546-3357", HourRate = 7f},
                            new Mechanic {Name =  "Chancellor Massey", Address =  "517-2477 Orci Avenue", Phone =  "(157) 785-5224", HourRate = 60f},
                            new Mechanic {Name =  "Orli Good", Address =  "192-9991 Dolor St.", Phone =  "(322) 587-4691", HourRate = 27f},
                    #region ...Mechanics
                            new Mechanic {Name =  "Ralph Lowe", Address =  "Ap #446-1114 Lectus St.", Phone =  "(423) 486-6931", HourRate = 33f},
                            new Mechanic {Name =  "Brianna Mcconnell", Address =  "281-3685 Ac Road", Phone =  "(120) 158-2816", HourRate = 5f},
                            new Mechanic {Name =  "Constance Velazquez", Address =  "9271 Donec St.", Phone =  "(515) 974-4680", HourRate = 34f},
                            new Mechanic {Name =  "Candice Mcclain", Address =  "5540 Sed Rd.", Phone =  "(251) 281-3651", HourRate = 59f},
                            new Mechanic {Name =  "Olivia Blackburn", Address =  "Ap #657-7493 Eu St.", Phone =  "(697) 334-9522", HourRate = 23f},
                            new Mechanic {Name =  "Clio Davidson", Address =  "Ap #603-7393 At, Road", Phone =  "(323) 154-7321", HourRate = 66f},
                            new Mechanic {Name =  "Germaine Steele", Address =  "101-9178 Gravida Ave", Phone =  "(724) 151-8504", HourRate = 59f},
                            new Mechanic {Name =  "Adrienne Schroeder", Address =  "595-6538 Lorem Rd.", Phone =  "(137) 988-4721", HourRate = 24f},
                            new Mechanic {Name =  "Omar Sawyer", Address =  "Ap #816-1278 Aliquet. Street", Phone =  "(707) 189-4218", HourRate = 38f},
                            new Mechanic {Name =  "Forrest Church", Address =  "148-9506 Magnis St.", Phone =  "(843) 449-8794", HourRate = 24f},
                            new Mechanic {Name =  "Hedda Valentine", Address =  "585-3990 Sit St.", Phone =  "(238) 815-6383", HourRate = 35f},
                            new Mechanic {Name =  "Charde May", Address =  "618-1895 Molestie St.", Phone =  "(688) 362-5886", HourRate = 69f},
                            new Mechanic {Name =  "Halee Wolfe", Address =  "477-9849 Velit. St.", Phone =  "(864) 723-9923", HourRate = 13f},
                            new Mechanic {Name =  "Griffin Wheeler", Address =  "P.O. Box 887, 8465 Leo. Av.", Phone =  "(199) 762-9356", HourRate = 21f},
                            new Mechanic {Name =  "Philip Patterson", Address =  "Ap #902-9898 Lorem Av.", Phone =  "(576) 432-8586", HourRate = 16f},
                            new Mechanic {Name =  "Daquan Russo", Address =  "9870 Elit, Rd.", Phone =  "(597) 780-6915", HourRate = 20f},
                            new Mechanic {Name =  "Deanna Holt", Address =  "Ap #743-1596 Cursus St.", Phone =  "(585) 447-3623", HourRate = 39f},
                            new Mechanic {Name =  "Catherine Palmer", Address =  "P.O. Box 731, 5687 Tempor Road", Phone =  "(613) 206-4158", HourRate = 26f},
                            new Mechanic {Name =  "Maisie Stephenson", Address =  "916-7229 Suspendisse Av.", Phone =  "(302) 405-1042", HourRate = 77f},
                            new Mechanic {Name =  "Hayley Hansen", Address =  "5322 Nulla Av.", Phone =  "(394) 648-1252", HourRate = 30f},
                            new Mechanic {Name =  "Brenden Christensen", Address =  "Ap #947-5937 Nibh. Av.", Phone =  "(148) 388-4119", HourRate = 51f},
                            new Mechanic {Name =  "Brock Paul", Address =  "9282 Suspendisse Street", Phone =  "(384) 166-2494", HourRate = 37f},
                            new Mechanic {Name =  "Laura Barnett", Address =  "Ap #192-7128 Convallis Road", Phone =  "(713) 601-1525", HourRate = 35f},
                            new Mechanic {Name =  "Fulton Gray", Address =  "Ap #387-7205 Metus St.", Phone =  "(754) 866-6850", HourRate = 72f},
                            new Mechanic {Name =  "Tana Campbell", Address =  "199-7438 Placerat Avenue", Phone =  "(166) 549-2997", HourRate = 17f},
                            new Mechanic {Name =  "Jin Cabrera", Address =  "908-1737 Vestibulum Avenue", Phone =  "(844) 895-1543", HourRate = 10f},
                            new Mechanic {Name =  "Lysandra Cantrell", Address =  "P.O. Box 643, 8596 Donec Avenue", Phone =  "(731) 861-9504", HourRate = 68f},
                            new Mechanic {Name =  "Yolanda Zamora", Address =  "2343 Cursus Rd.", Phone =  "(908) 574-5950", HourRate = 15f},
                            new Mechanic {Name =  "Olivia Simpson", Address =  "608-6434 Proin Street", Phone =  "(552) 399-0911", HourRate = 17f},
                            new Mechanic {Name =  "Autumn Cunningham", Address =  "6616 Mollis St.", Phone =  "(218) 128-6433", HourRate = 43f},
                            new Mechanic {Name =  "Miriam Jimenez", Address =  "Ap #739-7253 Ante Avenue", Phone =  "(556) 504-1248", HourRate = 25f},
                            new Mechanic {Name =  "Uriel Maddox", Address =  "1502 Eget St.", Phone =  "(862) 872-0256", HourRate = 9f},
                            new Mechanic {Name =  "Morgan Price", Address =  "9652 Quis, Av.", Phone =  "(901) 938-8275", HourRate = 31f},
                            new Mechanic {Name =  "Acton Henson", Address =  "P.O. Box 435, 7435 Aliquet. Street", Phone =  "(887) 375-8029", HourRate = 67f},
                            new Mechanic {Name =  "Summer Ryan", Address =  "2494 Donec Rd.", Phone =  "(633) 503-8773", HourRate = 57f},
                            new Mechanic {Name =  "Hanna Riggs", Address =  "P.O. Box 118, 2545 Et Av.", Phone =  "(604) 222-9701", HourRate = 12f},
                            new Mechanic {Name =  "Tamara Tyson", Address =  "P.O. Box 713, 6989 Ligula Rd.", Phone =  "(144) 743-0049", HourRate = 78f},
                            new Mechanic {Name =  "Ezra Morris", Address =  "979-9135 At Avenue", Phone =  "(528) 360-1265", HourRate = 41f},
                            new Mechanic {Name =  "Harriet Wilkins", Address =  "P.O. Box 570, 2709 A, St.", Phone =  "(768) 459-0170", HourRate = 6f},
                            new Mechanic {Name =  "Clayton Mcmillan", Address =  "P.O. Box 144, 4345 Risus. Street", Phone =  "(827) 856-9817", HourRate = 27f},
                            new Mechanic {Name =  "Oscar Gentry", Address =  "9443 Rhoncus. Avenue", Phone =  "(563) 763-3923", HourRate = 75f},
                            new Mechanic {Name =  "Amos Mendoza", Address =  "Ap #109-409 Nibh St.", Phone =  "(415) 200-5787", HourRate = 22f},
                            new Mechanic {Name =  "Cassady Schneider", Address =  "P.O. Box 817, 6278 Aenean Street", Phone =  "(410) 591-9968", HourRate = 49f},
                            new Mechanic {Name =  "Kermit Barton", Address =  "8510 Lorem St.", Phone =  "(488) 664-4358", HourRate = 68f},
                            new Mechanic {Name =  "Morgan Lang", Address =  "P.O. Box 568, 9416 Massa Road", Phone =  "(932) 228-5016", HourRate = 33f},
                            new Mechanic {Name =  "Keely Harper", Address =  "330-1700 Vivamus St.", Phone =  "(534) 975-2980", HourRate = 55f},
                            new Mechanic {Name =  "Carol Hughes", Address =  "4490 Mauris. Road", Phone =  "(300) 503-7012", HourRate = 34f},
                            new Mechanic {Name =  "Mark Lowe", Address =  "4522 Suspendisse Rd.", Phone =  "(299) 300-4106", HourRate = 64f},
                            new Mechanic {Name =  "Carissa Rosales", Address =  "Ap #914-358 Donec Street", Phone =  "(689) 771-2618", HourRate = 18f},
                            new Mechanic {Name =  "Akeem Richmond", Address =  "930-2806 Accumsan Rd.", Phone =  "(812) 337-3926", HourRate = 6f},
                            new Mechanic {Name =  "Bert Ferrell", Address =  "896 Est Rd.", Phone =  "(897) 143-0732", HourRate = 75f},
                            new Mechanic {Name =  "Ora Brooks", Address =  "P.O. Box 899, 2434 Et Ave", Phone =  "(876) 917-1888", HourRate = 8f},
                            new Mechanic {Name =  "Stone Snow", Address =  "Ap #575-3642 Adipiscing Street", Phone =  "(266) 328-7236", HourRate = 23f},
                            new Mechanic {Name =  "Ann Howell", Address =  "9785 Velit. Rd.", Phone =  "(303) 520-0794", HourRate = 32f},
                            new Mechanic {Name =  "Nicole Ward", Address =  "705-3476 Dolor. Rd.", Phone =  "(984) 778-4686", HourRate = 12f},
                            new Mechanic {Name =  "Ignatius Vincent", Address =  "Ap #670-1343 Cras Road", Phone =  "(848) 589-6133", HourRate = 70f},
                            new Mechanic {Name =  "Blair Alvarado", Address =  "9620 Sociis Street", Phone =  "(944) 534-7224", HourRate = 50f},
                            new Mechanic {Name =  "Hedy Howard", Address =  "P.O. Box 247, 7293 Placerat. Street", Phone =  "(417) 853-4172", HourRate = 74f},
                            new Mechanic {Name =  "Neville Lester", Address =  "P.O. Box 820, 5952 Justo. St.", Phone =  "(218) 120-2294", HourRate = 68f},
                            new Mechanic {Name =  "Sage Crosby", Address =  "Ap #952-6179 Dictum Avenue", Phone =  "(189) 908-6239", HourRate = 23f},
                            new Mechanic {Name =  "John Brock", Address =  "P.O. Box 794, 9025 Purus, Av.", Phone =  "(222) 667-4448", HourRate = 55f},
                            new Mechanic {Name =  "Addison Suarez", Address =  "P.O. Box 219, 8026 Enim. Avenue", Phone =  "(366) 498-8418", HourRate = 47f},
                            new Mechanic {Name =  "Macon Estrada", Address =  "Ap #410-5067 Ultrices Rd.", Phone =  "(470) 303-2183", HourRate = 51f},
                            new Mechanic {Name =  "Curran Sykes", Address =  "4219 Adipiscing Avenue", Phone =  "(905) 117-3667", HourRate = 8f},
                            new Mechanic {Name =  "Hollee Anthony", Address =  "P.O. Box 736, 3191 Odio Rd.", Phone =  "(107) 685-5697", HourRate = 10f},
                            new Mechanic {Name =  "Nola Whitfield", Address =  "532-5019 Eget, Street", Phone =  "(331) 869-4120", HourRate = 52f},
                            new Mechanic {Name =  "George Bolton", Address =  "515-6085 Quam Rd.", Phone =  "(854) 490-6228", HourRate = 74f},
                            new Mechanic {Name =  "Ora Roach", Address =  "2439 Ac St.", Phone =  "(171) 161-1162", HourRate = 31f},
                            new Mechanic {Name =  "Callie Whitley", Address =  "637-7024 Ut, Road", Phone =  "(806) 115-1068", HourRate = 8f},
                            new Mechanic {Name =  "Sierra Gonzales", Address =  "Ap #867-8732 Diam Road", Phone =  "(615) 469-3120", HourRate = 61f},
                            new Mechanic {Name =  "Lael Bradshaw", Address =  "571-812 Donec St.", Phone =  "(116) 571-1229", HourRate = 75f},
                            new Mechanic {Name =  "Helen Reyes", Address =  "Ap #292-7251 Pharetra Avenue", Phone =  "(547) 807-3539", HourRate = 34f},
                            new Mechanic {Name =  "Genevieve Merritt", Address =  "Ap #301-8674 Tellus. St.", Phone =  "(844) 116-9592", HourRate = 23f},
                            new Mechanic {Name =  "Charissa Lindsay", Address =  "Ap #935-2009 Risus. Avenue", Phone =  "(869) 452-6361", HourRate = 8f},
                            new Mechanic {Name =  "Mechelle Ellis", Address =  "9874 Aliquet. Rd.", Phone =  "(158) 103-7098", HourRate = 38f},
                            new Mechanic {Name =  "Kiara Valdez", Address =  "9626 Eros Rd.", Phone =  "(343) 677-4803", HourRate = 75f},
                            new Mechanic {Name =  "Alexis Santana", Address =  "393-4179 At, Rd.", Phone =  "(392) 449-9795", HourRate = 41f},
                            new Mechanic {Name =  "Winifred Fischer", Address =  "Ap #537-1270 Eleifend St.", Phone =  "(296) 728-6170", HourRate = 73f},
                            new Mechanic {Name =  "Molly Lewis", Address =  "2764 Aliquet Road", Phone =  "(980) 633-0641", HourRate = 41f},
                            new Mechanic {Name =  "Bree Bradley", Address =  "P.O. Box 277, 1805 Nullam St.", Phone =  "(851) 923-1384", HourRate = 42f},
                            new Mechanic {Name =  "Garrett David", Address =  "P.O. Box 201, 3518 Varius Ave", Phone =  "(901) 429-1705", HourRate = 20f},
                            new Mechanic {Name =  "David Livingston", Address =  "970-4183 Gravida Road", Phone =  "(422) 330-3064", HourRate = 60f},
                            new Mechanic {Name =  "Emma Herrera", Address =  "Ap #627-9621 Nec, Rd.", Phone =  "(664) 134-4813", HourRate = 10f},
                            new Mechanic {Name =  "Palmer Sims", Address =  "Ap #546-7073 Amet, Rd.", Phone =  "(426) 565-5090", HourRate = 76f},
                            new Mechanic {Name =  "Holmes Bush", Address =  "236-9581 Dui. St.", Phone =  "(242) 570-4212", HourRate = 61f},
                            new Mechanic {Name =  "Hunter Ramos", Address =  "4817 Nulla Av.", Phone =  "(652) 532-5442", HourRate = 46f},
                            new Mechanic {Name =  "Suki Herrera", Address =  "P.O. Box 882, 3377 Metus Road", Phone =  "(852) 358-6097", HourRate = 9f},
                            new Mechanic {Name =  "Oleg Barrera", Address =  "P.O. Box 160, 6602 A, Av.", Phone =  "(181) 140-9251", HourRate = 54f},
                            new Mechanic {Name =  "Fleur Kirby", Address =  "Ap #260-3088 Morbi Rd.", Phone =  "(324) 553-9930", HourRate = 59f},
                            new Mechanic {Name =  "Graham Stone", Address =  "Ap #400-4609 Id Street", Phone =  "(381) 692-0146", HourRate = 22f},
                            new Mechanic {Name =  "Aurora Gates", Address =  "Ap #418-8312 Ipsum Ave", Phone =  "(932) 961-2137", HourRate = 63f},
                            new Mechanic {Name =  "Zeus Alston", Address =  "Ap #105-9066 Ligula. St.", Phone =  "(175) 301-3947", HourRate = 22f},
                            new Mechanic {Name =  "Richard Christensen", Address =  "Ap #520-250 Risus. Rd.", Phone =  "(237) 509-9727", HourRate = 10f},
                            new Mechanic {Name =  "Jordan Byers", Address =  "8901 Pellentesque St.", Phone =  "(166) 439-3647", HourRate = 44f},
                            new Mechanic {Name =  "Oren Solomon", Address =  "340-6078 Donec Rd.", Phone =  "(988) 822-7914", HourRate = 58f},
                            new Mechanic {Name =  "Haley Finch", Address =  "Ap #575-315 Pede, St.", Phone =  "(174) 623-5837", HourRate = 22f},
                            new Mechanic {Name =  "Jonas Freeman", Address =  "P.O. Box 926, 5811 Mauris, St.", Phone =  "(816) 918-9650", HourRate = 42f}
                            #endregion // #region Mechanics
                        };
                var repairServices = new List<RepairService>
                        {
                            new RepairService {Name = "Clutch Adjustment", Price = 39.99},
                            new RepairService {Name = "Axle and Wheel Seal Installation", Price = 129.99},
                            new RepairService {Name = "D.O.T. Annual Inspection", Price = 89.99},
                    #region ...Services
                            new RepairService {Name = "Air Conditioning", Price = 129.99},
                            new RepairService {Name = "Alternator Installation", Price = 99.99},
                            new RepairService {Name = "Air Filter Inspection", Price = 44.99},
                            new RepairService {Name = "Water Pump Installation", Price = 549.99},
                            new RepairService {Name = "Electrical System Condition Inspection", Price = 49.99},
                            new RepairService {Name = "Jump Start on TA Lot", Price = 41.99},
                            new RepairService {Name = "Battery Charge (1/2 Hour)", Price = 33.99},
                            new RepairService {Name = "Battery Charge (1 Hour)", Price = 54.99},
                            new RepairService {Name = "Fuel or Oil Filter Replacement", Price = 12.99},
                            new RepairService {Name = "Headlamp Replacement", Price = 19.99},
                            new RepairService {Name = "Wiper Blade Replacement", Price = 7.99},
                            new RepairService {Name = "Trailer Hub Cap Replacement", Price = 34.99},
                            new RepairService {Name = "Trailer Leaf Spring Replacement", Price = 109.99},
                            new RepairService {Name = "2-Axle Alignment (AA2X)", Price = 139.99},
                            new RepairService {Name = "3-Axle Alignment (AA3Z)", Price = 209.99},
                            new RepairService {Name = "Alighment Check, No Adjustment (AACK)", Price = 79.99},
                            new RepairService {Name = "Center Steering Wheel (AASW)", Price = 45.99},
                            new RepairService {Name = "Steer Axle Alignment, Toe Only (AATO)", Price = 114.99},
                            new RepairService {Name = "Trailer Alignment (AATR)", Price = 114.99}
                            #endregion // Services
                        };
                var vendors = new List<Make>
                        {
                            new Make {Name = "American Coleman"},
                            new Make {Name = "Autocar Company (USA)"},
                            new Make {Name = "Bailey"},
                    #region ...Vendors
                    new Make {Name = "Brockway"},
                            new Make {Name = "Crane Carrier Corporation (USA)"},
                            new Make {Name = "Cline"},
                            new Make {Name = "Corbitt"},
                            new Make {Name = "DeSoto"},
                            new Make {Name = "Dina (Mexico)"},
                            new Make {Name = "E-One"},
                            new Make {Name = "Flextruc (Canada)"},
                            new Make {Name = "Freeman"},
                            new Make {Name = "Freightliner Trucks"},
                            new Make {Name = "Gersix (USA)"},
                            new Make {Name = "General Motors Canada"},
                            new Make {Name = "General Vehicle (or G.V.) (USA)"},
                            new Make {Name = "Hendrickson"},
                            new Make {Name = "Hug (USA)"},
                            new Make {Name = "Isuzu"},
                            new Make {Name = "Jeffedry Quad (USA)"},
                            new Make {Name = "Knox (USA)"},
                            new Make {Name = "Mack Trucks"},
                            new Make {Name = "Marmon"},
                            new Make {Name = "Moreland (USA)[citation needed]"},
                            new Make {Name = "Nissan"},
                            new Make {Name = "Kalmar Industries (formerly Ottawa) (yard switch trucks)"},
                            new Make {Name = "Paymaster[citation needed]"},
                            new Make {Name = "Pierce"},
                            new Make {Name = "Rapid (USA)"},
                            new Make {Name = "Reo (USA)"},
                            new Make {Name = "Riker (USA)[citation needed]"},
                            new Make {Name = "Stewart (USA)"},
                            new Make {Name = "Studebaker (USA)"},
                            new Make {Name = "Traffic (USA)"},
                            new Make {Name = "Volvo Trucks (different models for U.S. market)"},
                            new Make {Name = "White"},
                            new Make {Name = "Zeligson (USA)"}
                            #endregion // Vendors
                        };
                var random = new Random();
                var parts = new List<Part>
                        {
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 30))), Number= 12015, Description= "et nunc. Quisque ornare tortor", BuyPrice= 32.92, SellPrice= 58.20, Quantity= 19},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18804, Description= "aliquet. Proin velit. Sed malesuada augue", BuyPrice= 48.77, SellPrice= 73.99, Quantity= 19},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15082, Description= "mauris, aliquam eu, accumsan", BuyPrice= 34.92, SellPrice= 56.21, Quantity= 5},
                    #region ...Parts
                    new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15751, Description= "Aliquam ultrices iaculis odio. Nam interdum", BuyPrice= 31.06, SellPrice= 69.74, Quantity= 8},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16732, Description= "suscipit nonummy. Fusce fermentum", BuyPrice= 33.24, SellPrice= 73.37, Quantity= 12},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 11752, Description= "lectus justo eu arcu. Morbi sit amet massa. Quisque", BuyPrice= 47.96, SellPrice= 73.12, Quantity= 20},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 13239, Description= "ullamcorper eu, euismod ac, fermentum vel, mauris.", BuyPrice= 49.95, SellPrice= 51.83, Quantity= 2},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12553, Description= "cursus non, egestas a,", BuyPrice= 46.20, SellPrice= 63.08, Quantity= 20},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 17948, Description= "mollis dui, in sodales elit", BuyPrice= 32.08, SellPrice= 64.68, Quantity= 2},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18959, Description= "non ante bibendum ullamcorper. Duis", BuyPrice= 48.70, SellPrice= 61.31, Quantity= 20},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 10455, Description= "ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor. Nulla semper", BuyPrice= 33.26, SellPrice= 60.46, Quantity= 14},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14562, Description= "In condimentum. Donec at arcu. Vestibulum ante ipsum primis", BuyPrice= 49.00, SellPrice= 74.55, Quantity= 18},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15085, Description= "per conubia nostra, per inceptos hymenaeos. Mauris ut", BuyPrice= 38.32, SellPrice= 71.70, Quantity= 10},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12564, Description= "Duis cursus, diam at pretium aliquet, metus urna convallis", BuyPrice= 49.53, SellPrice= 70.54, Quantity= 7},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16578, Description= "dolor sit amet, consectetuer adipiscing elit. Etiam laoreet, libero et", BuyPrice= 44.05, SellPrice= 71.83, Quantity= 4},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14349, Description= "lacus. Aliquam rutrum lorem", BuyPrice= 38.46, SellPrice= 56.61, Quantity= 5},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18056, Description= "tempor, est ac mattis semper, dui lectus rutrum urna,", BuyPrice= 41.72, SellPrice= 67.69, Quantity= 3},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 11189, Description= "diam nunc, ullamcorper eu, euismod ac, fermentum vel, mauris.", BuyPrice= 34.19, SellPrice= 59.07, Quantity= 20},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14485, Description= "semper rutrum. Fusce dolor quam, elementum at,", BuyPrice= 35.56, SellPrice= 60.82, Quantity= 2},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14691, Description= "Nullam suscipit, est ac facilisis facilisis, magna tellus", BuyPrice= 43.68, SellPrice= 70.29, Quantity= 4},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 17853, Description= "Suspendisse sed dolor. Fusce mi lorem,", BuyPrice= 47.99, SellPrice= 63.11, Quantity= 7},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16417, Description= "elit, pellentesque a, facilisis non, bibendum sed,", BuyPrice= 37.85, SellPrice= 68.28, Quantity= 4},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 13014, Description= "leo. Vivamus nibh dolor, nonummy ac,", BuyPrice= 49.04, SellPrice= 68.78, Quantity= 5},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 17650, Description= "luctus felis purus ac tellus.", BuyPrice= 46.16, SellPrice= 62.69, Quantity= 10},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 10022, Description= "odio. Nam interdum enim non nisi. Aenean eget", BuyPrice= 37.29, SellPrice= 66.61, Quantity= 10},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 17697, Description= "massa lobortis ultrices. Vivamus rhoncus.", BuyPrice= 43.15, SellPrice= 59.10, Quantity= 1},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15754, Description= "quis diam luctus lobortis. Class aptent taciti sociosqu ad litora", BuyPrice= 32.11, SellPrice= 53.35, Quantity= 18},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 13702, Description= "Duis a mi fringilla mi lacinia", BuyPrice= 30.23, SellPrice= 62.08, Quantity= 4},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16294, Description= "justo sit amet nulla. Donec non justo. Proin non massa", BuyPrice= 39.39, SellPrice= 52.43, Quantity= 3},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14496, Description= "eget ipsum. Suspendisse sagittis. Nullam vitae diam. Proin dolor.", BuyPrice= 41.08, SellPrice= 73.99, Quantity= 19},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14921, Description= "scelerisque neque. Nullam nisl. Maecenas malesuada fringilla est. Mauris eu", BuyPrice= 41.50, SellPrice= 52.29, Quantity= 20},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 11210, Description= "vel arcu eu odio tristique pharetra. Quisque", BuyPrice= 41.09, SellPrice= 62.40, Quantity= 1},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18882, Description= "sit amet nulla. Donec non justo.", BuyPrice= 49.93, SellPrice= 50.00, Quantity= 1},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15596, Description= "pharetra. Nam ac nulla. In", BuyPrice= 30.63, SellPrice= 50.14, Quantity= 1},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12638, Description= "mattis velit justo nec ante. Maecenas mi felis, adipiscing fringilla,", BuyPrice= 30.02, SellPrice= 55.43, Quantity= 17},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16142, Description= "ultrices. Duis volutpat nunc sit amet metus.", BuyPrice= 44.86, SellPrice= 64.72, Quantity= 11},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 13830, Description= "natoque penatibus et magnis", BuyPrice= 30.48, SellPrice= 61.67, Quantity= 13},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12966, Description= "per inceptos hymenaeos. Mauris ut quam vel", BuyPrice= 35.48, SellPrice= 66.86, Quantity= 6},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 17369, Description= "Vivamus nibh dolor, nonummy ac, feugiat non, lobortis quis,", BuyPrice= 40.47, SellPrice= 70.67, Quantity= 8},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 13857, Description= "auctor, velit eget laoreet posuere, enim", BuyPrice= 44.79, SellPrice= 58.10, Quantity= 18},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 13953, Description= "ante ipsum primis in faucibus orci luctus et", BuyPrice= 36.02, SellPrice= 74.81, Quantity= 10},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18952, Description= "metus facilisis lorem tristique aliquet. Phasellus fermentum convallis", BuyPrice= 30.06, SellPrice= 67.58, Quantity= 8},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16557, Description= "amet ornare lectus justo", BuyPrice= 41.15, SellPrice= 63.32, Quantity= 17},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 10846, Description= "Quisque libero lacus, varius et, euismod et, commodo at,", BuyPrice= 37.81, SellPrice= 71.31, Quantity= 17},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 10288, Description= "adipiscing ligula. Aenean gravida nunc sed", BuyPrice= 43.70, SellPrice= 71.83, Quantity= 13},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15661, Description= "non arcu. Vivamus sit amet risus. Donec egestas.", BuyPrice= 31.01, SellPrice= 55.05, Quantity= 10},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12373, Description= "magnis dis parturient montes, nascetur ridiculus mus.", BuyPrice= 40.37, SellPrice= 72.69, Quantity= 5},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12313, Description= "nibh lacinia orci, consectetuer euismod est arcu ac", BuyPrice= 35.78, SellPrice= 56.61, Quantity= 13},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 10829, Description= "Cras lorem lorem, luctus ut, pellentesque", BuyPrice= 44.41, SellPrice= 67.74, Quantity= 16},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18883, Description= "dolor dolor, tempus non, lacinia at, iaculis quis, pede. Praesent", BuyPrice= 45.15, SellPrice= 69.35, Quantity= 6},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15496, Description= "luctus sit amet, faucibus ut, nulla.", BuyPrice= 37.16, SellPrice= 74.40, Quantity= 8},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16736, Description= "porttitor scelerisque neque. Nullam", BuyPrice= 30.04, SellPrice= 56.36, Quantity= 1},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 17245, Description= "dapibus gravida. Aliquam tincidunt, nunc ac mattis", BuyPrice= 35.62, SellPrice= 74.95, Quantity= 2},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 11970, Description= "accumsan laoreet ipsum. Curabitur consequat,", BuyPrice= 44.26, SellPrice= 50.73, Quantity= 9},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18564, Description= "in magna. Phasellus dolor", BuyPrice= 31.92, SellPrice= 65.49, Quantity= 19},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15043, Description= "lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit elit", BuyPrice= 32.78, SellPrice= 54.51, Quantity= 19},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 10912, Description= "pede blandit congue. In scelerisque scelerisque dui.", BuyPrice= 49.08, SellPrice= 71.94, Quantity= 6},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18376, Description= "non enim. Mauris quis turpis vitae purus gravida", BuyPrice= 44.72, SellPrice= 73.61, Quantity= 17},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14792, Description= "tincidunt aliquam arcu. Aliquam ultrices iaculis odio. Nam interdum enim", BuyPrice= 45.18, SellPrice= 69.07, Quantity= 17},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16564, Description= "odio sagittis semper. Nam tempor diam dictum", BuyPrice= 36.13, SellPrice= 53.43, Quantity= 9},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14716, Description= "nulla. Integer vulputate, risus a ultricies", BuyPrice= 40.09, SellPrice= 50.53, Quantity= 10},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 13784, Description= "at sem molestie sodales. Mauris blandit", BuyPrice= 37.18, SellPrice= 67.77, Quantity= 3},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 17150, Description= "ultrices a, auctor non, feugiat nec, diam. Duis", BuyPrice= 46.73, SellPrice= 62.19, Quantity= 11},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 13433, Description= "leo. Cras vehicula aliquet libero. Integer", BuyPrice= 38.62, SellPrice= 61.74, Quantity= 7},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18619, Description= "ridiculus mus. Proin vel arcu eu odio tristique pharetra.", BuyPrice= 39.57, SellPrice= 70.56, Quantity= 2},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14620, Description= "orci. Ut sagittis lobortis", BuyPrice= 41.75, SellPrice= 61.60, Quantity= 12},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16835, Description= "nibh dolor, nonummy ac, feugiat", BuyPrice= 41.07, SellPrice= 63.23, Quantity= 5},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16424, Description= "non quam. Pellentesque habitant morbi tristique senectus et", BuyPrice= 47.24, SellPrice= 53.72, Quantity= 20},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12467, Description= "Vivamus nisi. Mauris nulla. Integer urna. Vivamus molestie dapibus", BuyPrice= 39.54, SellPrice= 66.55, Quantity= 12},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14962, Description= "vitae, aliquet nec, imperdiet nec,", BuyPrice= 30.51, SellPrice= 65.81, Quantity= 3},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 13181, Description= "mi. Duis risus odio, auctor vitae, aliquet", BuyPrice= 48.91, SellPrice= 70.78, Quantity= 18},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15192, Description= "dolor quam, elementum at, egestas a, scelerisque sed,", BuyPrice= 41.35, SellPrice= 50.71, Quantity= 18},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18641, Description= "odio. Phasellus at augue id ante dictum cursus. Nunc mauris", BuyPrice= 42.46, SellPrice= 53.15, Quantity= 19},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 13124, Description= "aliquet nec, imperdiet nec, leo. Morbi neque tellus, imperdiet non,", BuyPrice= 46.00, SellPrice= 59.87, Quantity= 12},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12791, Description= "et ultrices posuere cubilia Curae; Donec tincidunt. Donec vitae erat", BuyPrice= 49.67, SellPrice= 62.20, Quantity= 9},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18178, Description= "felis orci, adipiscing non, luctus sit amet,", BuyPrice= 38.15, SellPrice= 73.98, Quantity= 6},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14401, Description= "molestie tellus. Aenean egestas", BuyPrice= 30.28, SellPrice= 60.05, Quantity= 9},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15497, Description= "fermentum fermentum arcu. Vestibulum", BuyPrice= 49.61, SellPrice= 68.10, Quantity= 19},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 18304, Description= "Phasellus in felis. Nulla tempor augue ac", BuyPrice= 42.57, SellPrice= 71.34, Quantity= 12},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12295, Description= "ac mi eleifend egestas. Sed pharetra, felis", BuyPrice= 47.18, SellPrice= 54.66, Quantity= 2},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12353, Description= "risus varius orci, in consequat enim diam vel arcu.", BuyPrice= 30.71, SellPrice= 58.34, Quantity= 11},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 11754, Description= "nunc sed libero. Proin sed turpis nec mauris blandit mattis.", BuyPrice= 45.85, SellPrice= 68.04, Quantity= 19},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 10280, Description= "dignissim pharetra. Nam ac", BuyPrice= 31.00, SellPrice= 59.21, Quantity= 20},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15400, Description= "id magna et ipsum cursus vestibulum. Mauris", BuyPrice= 32.69, SellPrice= 56.70, Quantity= 7},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 10724, Description= "Proin ultrices. Duis volutpat", BuyPrice= 30.23, SellPrice= 56.36, Quantity= 18},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16300, Description= "pharetra, felis eget varius ultrices,", BuyPrice= 49.70, SellPrice= 61.75, Quantity= 5},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16783, Description= "eu lacus. Quisque imperdiet, erat nonummy ultricies ornare, elit", BuyPrice= 49.88, SellPrice= 52.68, Quantity= 3},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15770, Description= "lorem semper auctor. Mauris vel turpis.", BuyPrice= 45.76, SellPrice= 59.30, Quantity= 11},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 11380, Description= "elit, a feugiat tellus lorem eu metus. In lorem.", BuyPrice= 32.04, SellPrice= 67.54, Quantity= 9},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14631, Description= "feugiat. Lorem ipsum dolor sit amet,", BuyPrice= 34.08, SellPrice= 52.87, Quantity= 14},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 15985, Description= "Phasellus dolor elit, pellentesque a, facilisis non, bibendum", BuyPrice= 40.78, SellPrice= 56.92, Quantity= 4},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14000, Description= "lectus justo eu arcu. Morbi sit amet massa. Quisque", BuyPrice= 41.91, SellPrice= 72.49, Quantity= 2},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 10741, Description= "consectetuer adipiscing elit. Aliquam auctor,", BuyPrice= 37.33, SellPrice= 63.83, Quantity= 2},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12384, Description= "at fringilla purus mauris", BuyPrice= 37.87, SellPrice= 50.05, Quantity= 2},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12175, Description= "nec metus facilisis lorem", BuyPrice= 45.82, SellPrice= 57.39, Quantity= 11},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 11412, Description= "dui nec urna suscipit nonummy. Fusce", BuyPrice= 37.11, SellPrice= 59.31, Quantity= 3},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 14893, Description= "sit amet, faucibus ut, nulla.", BuyPrice= 44.53, SellPrice= 73.66, Quantity= 8},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 10235, Description= "Suspendisse sagittis. Nullam vitae", BuyPrice= 38.19, SellPrice= 52.31, Quantity= 12},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 12996, Description= "Morbi quis urna. Nunc", BuyPrice= 33.25, SellPrice= 55.60, Quantity= 13},
                            new Part{Date = DateTime.Today.Subtract(TimeSpan.FromDays(random.Next(0, 31))), Number= 16953, Description= "mus. Aenean eget magna. Suspendisse tristique neque venenatis lacus. Etiam", BuyPrice= 49.82, SellPrice= 55.43, Quantity= 10}
                            #endregion // Parts
                        };

                // add some movies
                var owners = new List<Owner>
                        {
                            new Owner{Name= "Mechelle Cote", Address= "650-8776 Ullamcorper. Street", Phone= "(378) 392-1047"},
                            new Owner{Name= "Maisie Matthews", Address= "Ap #942-8958 Nulla Av.", Phone= "(301) 131-3764"},
                            new Owner{Name= "Dara Boyer", Address= "749 Cras Street", Phone= "(791) 873-6374"},
                    #region ...Owners
                    new Owner{Name= "Glenna Travis", Address= "822-2999 Mauris Rd.", Phone= "(866) 801-7667"},
                            new Owner{Name= "Maxine Adkins", Address= "Ap #480-4501 In Street", Phone= "(130) 171-4788"},
                            new Owner{Name= "Cassidy Ochoa", Address= "210-5901 Est Ave", Phone= "(722) 813-4438"},
                            new Owner{Name= "Lyle Peck", Address= "Ap #778-7769 Cum Av.", Phone= "(940) 988-6261"},
                            new Owner{Name= "Ivan Mccoy", Address= "819-4962 Placerat, Avenue", Phone= "(744) 563-2708"},
                            new Owner{Name= "Jeremy Thompson", Address= "4343 Ultrices Avenue", Phone= "(678) 481-4281"},
                            new Owner{Name= "Jessica Huffman", Address= "Ap #697-1415 Lectus Rd.", Phone= "(734) 428-8978"},
                            new Owner{Name= "Ivy Yang", Address= "4991 Sollicitudin St.", Phone= "(289) 996-4709"},
                            new Owner{Name= "Cameron Mejia", Address= "P.O. Box 932, 8930 Donec St.", Phone= "(169) 850-9441"},
                            new Owner{Name= "Ingrid Everett", Address= "P.O. Box 975, 8947 Vestibulum St.", Phone= "(538) 469-4786"},
                            new Owner{Name= "Adam Garrison", Address= "993-5478 Aenean Av.", Phone= "(339) 890-7399"},
                            new Owner{Name= "Galena Daugherty", Address= "P.O. Box 213, 291 Porttitor Avenue", Phone= "(518) 583-6575"},
                            new Owner{Name= "Oprah Cruz", Address= "Ap #369-4286 Volutpat. Street", Phone= "(232) 776-7213"},
                            new Owner{Name= "Sage Tillman", Address= "Ap #807-8714 Proin St.", Phone= "(936) 878-8676"},
                            new Owner{Name= "Reece Stanton", Address= "P.O. Box 143, 624 Parturient St.", Phone= "(406) 779-8301"},
                            new Owner{Name= "Alec Jacobson", Address= "P.O. Box 766, 813 Arcu Street", Phone= "(970) 995-9728"},
                            new Owner{Name= "Axel Anderson", Address= "P.O. Box 225, 4880 Mollis Road", Phone= "(269) 303-3810"},
                            new Owner{Name= "Linda Nolan", Address= "P.O. Box 825, 6560 Fringilla. Road", Phone= "(953) 103-0632"},
                            new Owner{Name= "Chanda Robinson", Address= "822-7388 Curabitur St.", Phone= "(109) 506-0761"},
                            new Owner{Name= "Risa Benson", Address= "282 Neque Rd.", Phone= "(142) 846-1785"},
                            new Owner{Name= "Cassandra Rodgers", Address= "886-611 At, Street", Phone= "(947) 169-0844"},
                            new Owner{Name= "Veronica Andrews", Address= "P.O. Box 555, 5836 Tincidunt Rd.", Phone= "(762) 330-1766"},
                            new Owner{Name= "Grant Pickett", Address= "P.O. Box 454, 4804 A, Rd.", Phone= "(641) 197-8698"},
                            new Owner{Name= "Kelsie Conway", Address= "Ap #433-7245 Odio. Rd.", Phone= "(411) 271-0587"},
                            new Owner{Name= "Cherokee Elliott", Address= "Ap #599-6894 Eu Street", Phone= "(987) 576-9945"},
                            new Owner{Name= "Ayanna Sullivan", Address= "Ap #111-1131 Bibendum Rd.", Phone= "(139) 473-7289"},
                            new Owner{Name= "Valentine James", Address= "Ap #975-5477 Mauris St.", Phone= "(977) 412-9789"},
                            new Owner{Name= "Ori Leon", Address= "Ap #106-910 Nec St.", Phone= "(476) 430-3960"},
                            new Owner{Name= "Rachel Lane", Address= "4961 Sed Ave", Phone= "(437) 675-7096"},
                            new Owner{Name= "Silas Summers", Address= "661-9536 Pede Ave", Phone= "(642) 632-7328"},
                            new Owner{Name= "Colorado Simon", Address= "216-1047 Fusce St.", Phone= "(564) 585-1737"},
                            new Owner{Name= "Beverly Everett", Address= "P.O. Box 749, 9249 Imperdiet Ave", Phone= "(256) 522-1216"},
                            new Owner{Name= "Rhoda Solomon", Address= "Ap #271-159 Commodo St.", Phone= "(969) 594-8129"},
                            new Owner{Name= "Libby Nielsen", Address= "Ap #411-3205 Consectetuer Ave", Phone= "(201) 319-5508"},
                            new Owner{Name= "Clarke Sharpe", Address= "P.O. Box 328, 6576 Vel, St.", Phone= "(595) 685-7733"},
                            new Owner{Name= "Rebekah Dean", Address= "P.O. Box 348, 374 Vitae Street", Phone= "(432) 580-0588"},
                            new Owner{Name= "September Schultz", Address= "188-9392 Eu Rd.", Phone= "(925) 684-1878"},
                            new Owner{Name= "Julie Molina", Address= "P.O. Box 178, 2937 Ipsum. Rd.", Phone= "(312) 969-1835"},
                            new Owner{Name= "Whitney Fox", Address= "Ap #364-3221 Augue Av.", Phone= "(237) 823-0485"},
                            new Owner{Name= "Ishmael Rose", Address= "Ap #431-6942 Purus St.", Phone= "(283) 184-5236"},
                            new Owner{Name= "Bree Lester", Address= "P.O. Box 863, 7727 Vel St.", Phone= "(127) 513-8272"},
                            new Owner{Name= "Gavin Sanders", Address= "5175 Ligula. Avenue", Phone= "(419) 556-7351"},
                            new Owner{Name= "Micah Vincent", Address= "485-7371 Mauris Road", Phone= "(545) 616-6991"},
                            new Owner{Name= "Hillary Sheppard", Address= "Ap #671-9746 Non Road", Phone= "(522) 706-0230"},
                            new Owner{Name= "Beatrice Holmes", Address= "7625 Lacus. Rd.", Phone= "(362) 996-4944"},
                            new Owner{Name= "Flynn Mclaughlin", Address= "1024 Nisl. Av.", Phone= "(766) 644-3900"},
                            new Owner{Name= "Georgia Bradley", Address= "P.O. Box 212, 3660 Lorem Street", Phone= "(185) 205-7053"},
                            new Owner{Name= "Destiny Zamora", Address= "P.O. Box 740, 2047 Ac Ave", Phone= "(262) 146-5073"},
                            new Owner{Name= "Taylor Skinner", Address= "756-3585 Sed Ave", Phone= "(741) 224-1320"},
                            new Owner{Name= "Dalton Park", Address= "9543 Enim St.", Phone= "(166) 836-3701"},
                            new Owner{Name= "Brian Cantu", Address= "368 Justo Street", Phone= "(317) 870-7891"},
                            new Owner{Name= "Shafira Bowman", Address= "4621 Nulla St.", Phone= "(581) 491-1292"},
                            new Owner{Name= "Sean Rodgers", Address= "1800 Hendrerit. St.", Phone= "(501) 415-6445"},
                            new Owner{Name= "Quentin Clemons", Address= "105-1510 Cursus. Ave", Phone= "(675) 109-0612"},
                            new Owner{Name= "Jermaine Robbins", Address= "Ap #704-1432 Lacinia Avenue", Phone= "(571) 775-6747"},
                            new Owner{Name= "Rigel Steele", Address= "Ap #165-4443 Arcu. Street", Phone= "(612) 599-0845"},
                            new Owner{Name= "Bert Forbes", Address= "Ap #986-8882 Nisi. Av.", Phone= "(964) 200-1381"},
                            new Owner{Name= "Whoopi Shepherd", Address= "Ap #842-6421 Donec Av.", Phone= "(521) 470-2050"},
                            new Owner{Name= "Cathleen Ingram", Address= "1093 Tristique Av.", Phone= "(649) 339-3441"},
                            new Owner{Name= "Jamal Snider", Address= "217-4721 Risus, St.", Phone= "(937) 211-5896"},
                            new Owner{Name= "Allistair Ortiz", Address= "137-3998 Massa Av.", Phone= "(800) 529-2058"},
                            new Owner{Name= "Matthew Coffey", Address= "266-7801 Blandit Street", Phone= "(408) 631-8628"},
                            new Owner{Name= "Breanna Sutton", Address= "P.O. Box 836, 9769 Non Street", Phone= "(776) 341-3944"},
                            new Owner{Name= "Shad Tanner", Address= "Ap #170-7974 Mi St.", Phone= "(259) 729-4900"},
                            new Owner{Name= "Kai Kirk", Address= "P.O. Box 513, 9244 Turpis Rd.", Phone= "(450) 298-1097"},
                            new Owner{Name= "Justine Wolf", Address= "3134 In, Rd.", Phone= "(526) 808-0496"},
                            new Owner{Name= "Xanthus French", Address= "P.O. Box 423, 3890 Neque Rd.", Phone= "(239) 796-9720"},
                            new Owner{Name= "Cassidy Love", Address= "3854 Lorem St.", Phone= "(897) 182-4500"},
                            new Owner{Name= "Risa Pollard", Address= "428-8815 Duis St.", Phone= "(778) 850-6687"},
                            new Owner{Name= "Suki Morrison", Address= "463-2281 Mus. Av.", Phone= "(536) 374-1619"},
                            new Owner{Name= "Blair Malone", Address= "Ap #742-3311 Donec Ave", Phone= "(690) 182-8916"},
                            new Owner{Name= "Madeson Reeves", Address= "P.O. Box 243, 7176 Eleifend, Ave", Phone= "(173) 472-7736"},
                            new Owner{Name= "Wanda Hicks", Address= "4254 Eu St.", Phone= "(959) 375-8395"},
                            new Owner{Name= "Whitney Petersen", Address= "216-5092 Diam. Avenue", Phone= "(342) 905-8793"},
                            new Owner{Name= "Damon Bell", Address= "P.O. Box 142, 8634 Rutrum Rd.", Phone= "(190) 349-7934"},
                            new Owner{Name= "Melinda Fry", Address= "Ap #416-262 Pede. St.", Phone= "(912) 712-9977"},
                            new Owner{Name= "Erin Tran", Address= "6557 Varius St.", Phone= "(264) 534-1801"},
                            new Owner{Name= "Jillian Barker", Address= "5153 Orci Rd.", Phone= "(192) 299-8493"},
                            new Owner{Name= "Maxine Wilkerson", Address= "Ap #334-3332 Aliquet. Avenue", Phone= "(977) 480-5373"},
                            new Owner{Name= "Jeanette Hensley", Address= "8122 A, St.", Phone= "(569) 913-3161"},
                            new Owner{Name= "Victoria Larson", Address= "P.O. Box 946, 9556 Lobortis Street", Phone= "(352) 677-9184"},
                            new Owner{Name= "Kai Hamilton", Address= "7981 Ac Av.", Phone= "(226) 651-4024"},
                            new Owner{Name= "Hu Burke", Address= "705-5210 Aenean Street", Phone= "(653) 754-5065"},
                            new Owner{Name= "Ignatius Houston", Address= "Ap #877-2147 Dapibus Street", Phone= "(273) 309-5113"},
                            new Owner{Name= "Raphael Emerson", Address= "7962 A Avenue", Phone= "(256) 277-6629"},
                            new Owner{Name= "Samantha Lara", Address= "450-3211 Lobortis Rd.", Phone= "(455) 877-6320"},
                            new Owner{Name= "Emma Barr", Address= "1929 Pharetra Road", Phone= "(132) 955-6272"},
                            new Owner{Name= "Minerva Nguyen", Address= "P.O. Box 502, 8934 Nunc Rd.", Phone= "(402) 380-2540"},
                            new Owner{Name= "Virginia Manning", Address= "7627 Consectetuer Avenue", Phone= "(670) 451-7408"},
                            new Owner{Name= "Dominique Marshall", Address= "Ap #746-9674 Sagittis Av.", Phone= "(550) 695-2323"},
                            new Owner{Name= "Ethan Davis", Address= "974-4005 Curabitur Avenue", Phone= "(766) 979-2517"},
                            new Owner{Name= "Aretha Camacho", Address= "947-7242 Faucibus Av.", Phone= "(883) 612-2819"},
                            new Owner{Name= "Demetria Kennedy", Address= "Ap #728-644 Lorem, Ave", Phone= "(690) 543-9291"},
                            new Owner{Name= "Amelia Alvarez", Address= "P.O. Box 973, 5478 Nulla Street", Phone= "(907) 540-7929"},
                            new Owner{Name= "Joel Franco", Address= "Ap #841-9048 Amet Street", Phone= "(496) 619-0357"},
                            new Owner{Name= "Ann Foreman", Address= "4755 Ante Rd.", Phone= "(722) 241-0521"},
                            new Owner{Name= "Davis Acevedo", Address= "P.O. Box 320, 1023 Sit Rd.", Phone= "(832) 107-2911"}
                            #endregion // Owners
                        };
                dbContext.Makers.AddRange(vendors);
                dbContext.SaveChanges();
                var makers = dbContext.Makers.ToList();

                var truckTypes = new List<TruckType>
                        {
                            new TruckType {Name = "Mini Truck", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Minivan", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Sport utility vehicle", Make = makers[random.Next(0, makers.Count)]},
                    #region ...TruckTypes
                    new TruckType {Name = "Canopy express", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Pickup truck", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Panel truck", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Cab-forward", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Tow truck", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Panel van", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Sedan delivery", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Box truck", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Van", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Cutaway van chassis", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Medium Duty Truck such as Ford F-650 in North America", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Medium Standard Truck", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Platform truck", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Flatbed truck (may also be light duty trucks)", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Firetruck (may also be a heavy truck)", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Recreational Vehicle or Motorhome", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Concrete transport truck (cement mixer)", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Mobile crane", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Dump truck", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Garbage truck", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Log carrier", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Refrigerator truck", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Tractor unit", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Tank truck", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Ballast tractor", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Heavy hauler", Make = makers[random.Next(0, makers.Count)]},
                            new TruckType {Name = "Haul truck", Make = makers[random.Next(0, makers.Count)]}
                            #endregion // TruckTypes
                        };




                dbContext.Mechanics.AddRange(mechanics);
                dbContext.RepairServices.AddRange(repairServices);
                dbContext.TruckTypes.AddRange(truckTypes);
                dbContext.Parts.AddRange(parts);
                dbContext.Owners.AddRange(owners);

                var trailers = new List<Trailer>
                {
                    new Trailer {Number = 50003, Owner = owners[random.Next(0, owners.Count)]},
                    new Trailer {Number = 50004, Owner = owners[random.Next(0, owners.Count)]},
                    new Trailer {Number = 50005, Owner = owners[random.Next(0, owners.Count)]}
                };
                dbContext.Trailers.AddRange(trailers);

                var trucks = new List<Truck>()
                {
                    new Truck {
                        Number = 20001,
                        Owner = owners[random.Next(0, owners.Count)],
                        Trailers = new List<Trailer>
                        {
                            trailers[random.Next(0, trailers.Count)]
                        },
                        Milage = 369,
                        TireSize = 35,
                        TruckType = truckTypes[random.Next(0, truckTypes.Count)],
                        VinNumber = "12345678901234567",
                    }
                };
                dbContext.Trucks.AddRange(trucks);

                var fixes = new List<Fix>
                        {
                            new Fix {
                                Date = DateTime.Now,
                                FlatRate = random.Next(100, 1000),
                                IsFlatRate = true,
                                IsPaid = true,
                                IsTruck = false,
                                Trailer = trailers[random.Next(0, trailers.Count)],
                                Labour = random.Next(10,20),
                                Milage = random.Next(50,350),
                                Note = "N0te"
                            }
                        };
                dbContext.Fixes.AddRange(fixes);

                var m2f = new List<Mechanic2Fix>
                        {
                            new Mechanic2Fix { Fix = fixes[random.Next(0, fixes.Count())], Mechanic = mechanics[random.Next(0, mechanics.Count())]},
                            new Mechanic2Fix { Fix = fixes[random.Next(0, fixes.Count())], Mechanic = mechanics[random.Next(0, mechanics.Count())]}
                        };
                var s2f = new List<Service2Fix>
                        {
                            new Service2Fix { Fix = fixes[random.Next(0, fixes.Count())], RepairService = repairServices[random.Next(0, repairServices.Count())]},
                            new Service2Fix { Fix = fixes[random.Next(0, fixes.Count())], RepairService = repairServices[random.Next(0, repairServices.Count())]}
                        };
                var p2f = new List<Part2Fix>
                        {
                            new Part2Fix { Fix = fixes[random.Next(0, fixes.Count())], Part = parts[random.Next(0, parts.Count())], UsedPartsCount = random.Next(1,4)},
                            new Part2Fix { Fix = fixes[random.Next(0, fixes.Count())], Part = parts[random.Next(0, parts.Count())], UsedPartsCount = random.Next(1,4)}
                        };

                dbContext.Mechanic2Fixes.AddRange(m2f);
                dbContext.Service2Fixes.AddRange(s2f);
                dbContext.Part2Fixes.AddRange(p2f);

                dbContext.SaveChanges();
            }
        }
    }
}
