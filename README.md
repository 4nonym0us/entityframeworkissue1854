# README #

Repo that reproduces Enttiy Framework Issue [#1854](https://github.com/aspnet/EntityFramework/issues/1854#issuecomment-115359965)

**Do not forget to change connection string.**.

Exception:
```
An exception of type 'System.InvalidOperationException' occurred in EntityFramework.Core.dll but was not handled in user code

Additional information: Invalid attempt to read when no data is present
```
StackTrace:

```

   in System.Data.SqlClient.SqlDataReader.CheckDataIsReady(Int32 columnIndex, Boolean allowPartiallyReadColumn, Boolean permitAsync, String methodName)
   in System.Data.SqlClient.SqlDataReader.TryReadColumn(Int32 i, Boolean setTimeout, Boolean allowPartiallyReadColumn)
   in System.Data.SqlClient.SqlDataReader.GetValues(Object[] values)
   in Microsoft.Data.Entity.Relational.RelationalObjectArrayValueReader..ctor(DbDataReader dataReader)
   in Microsoft.Data.Entity.Relational.RelationalObjectArrayValueReaderFactory.Create(DbDataReader dataReader)
   in Microsoft.Data.Entity.Relational.Query.RelationalQueryContext.CreateValueReader(Int32 readerIndex)
   in Microsoft.Data.Entity.Relational.Query.QueryMethodProvider.ReferenceIncludeRelatedValuesStrategy.<GetRelatedValues>d__5.MoveNext()
   in System.Linq.Enumerable.WhereSelectEnumerableIterator`2.MoveNext()
   in System.Linq.Enumerable.WhereEnumerableIterator`1.MoveNext()
   in System.Collections.Generic.List`1..ctor(IEnumerable`1 collection)
   in System.Linq.Enumerable.ToList[TSource](IEnumerable`1 source)
   in Microsoft.Data.Entity.Query.QueryBuffer.Include(Object entity, IReadOnlyList`1 navigationPath, IReadOnlyList`1 relatedEntitiesLoaders, Int32 currentNavigationIndex, Boolean querySourceRequiresTracking)
   in Microsoft.Data.Entity.Query.QueryBuffer.<>c__DisplayClass12_0.<Include>b__0(EntityLoadInfo eli)
   in System.Linq.Enumerable.<>c__DisplayClass2`3.<CombineSelectors>b__3(TSource x)
   in System.Linq.Enumerable.WhereSelectEnumerableIterator`2.MoveNext()
   in System.Linq.Enumerable.WhereEnumerableIterator`1.MoveNext()
   in System.Collections.Generic.List`1..ctor(IEnumerable`1 collection)
   in System.Linq.Enumerable.ToList[TSource](IEnumerable`1 source)
   in Microsoft.Data.Entity.Query.QueryBuffer.Include(Object entity, IReadOnlyList`1 navigationPath, IReadOnlyList`1 relatedEntitiesLoaders, Int32 currentNavigationIndex, Boolean querySourceRequiresTracking)
   in Microsoft.Data.Entity.Query.QueryBuffer.Include(Object entity, IReadOnlyList`1 navigationPath, IReadOnlyList`1 relatedEntitiesLoaders, Boolean querySourceRequiresTracking)
   in Microsoft.Data.Entity.Relational.Query.QueryMethodProvider.<>c__DisplayClass11_0`1.<_Include>b__2(T qss)
   in System.Linq.Enumerable.WhereSelectEnumerableIterator`2.MoveNext()
   in Microsoft.Data.Entity.Relational.Utilities.EnumerableExtensions.<Finally>d__0`1.MoveNext()
   in System.Linq.Enumerable.WhereSelectEnumerableIterator`2.MoveNext()
   in Microsoft.Data.Entity.Relational.Utilities.EnumerableExtensions.<Finally>d__0`1.MoveNext()
   in System.Linq.Enumerable.<SelectManyIterator>d__1`2.MoveNext()
   in System.Linq.Enumerable.WhereSelectEnumerableIterator`2.MoveNext()
   in Microsoft.Data.Entity.Query.LinqOperatorProvider.ExceptionInterceptor`1.EnumeratorExceptionInterceptor.MoveNext()
   in System.Collections.Generic.List`1..ctor(IEnumerable`1 collection)
   in System.Linq.Enumerable.ToList[TSource](IEnumerable`1 source)
   in EntityFrameworkIssue1854.Program.GetData() in E:\git\EntityFrameworkIssue1854\src\EntityFrameworkIssue1854\Program.cs:line 35
   in EntityFrameworkIssue1854.Program.Main(String[] args) in E:\git\EntityFrameworkIssue1854\src\EntityFrameworkIssue1854\Program.cs:line 16
```